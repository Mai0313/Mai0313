### Hi there 👋
<p align="center">
  <a href="https://github.com/vn7n24fzkq/github-profile-summary-cards">
    <img height="198px" src="https://github-profile-summary-cards.vercel.app/api/cards/profile-details?username=Mai0313&theme=radical" />
  </a>
  <a href="https://github.com/anuraghazra/github-readme-stats">
    <img height="288px" src="https://github-readme-stats.vercel.app/api?username=Mai0313&size_weight=1&count_weight=1&rank_icon=github&show_icons=true&theme=radical" />
  </a>
</p>
<p align="center">
  <a href="https://github.com/vn7n24fzkq/github-profile-summary-cards">
    <img height="250px" src="https://github-profile-summary-cards.vercel.app/api/cards/productive-time?username=Mai0313&theme=radical" />
  </a>
  <a href="https://github.com/anuraghazra/github-readme-stats">
    <img height="250px" src="https://github-readme-stats.vercel.app/api/top-langs/?username=Mai0313&size_weight=0.5&count_weight=0.5&show_icons=true&theme=radical" />
  </a>
</p>

<p align='center'>
  💻 PC <br/><br/>
  <img src="https://img.shields.io/badge/windows-%230078D6.svg?&style=for-the-badge&logo=windows&logoColor=white" />
  <img src="https://img.shields.io/badge/intel-core%20i9%2013th-%230071C5.svg?&style=for-the-badge&logo=intel&logoColor=white" />
  <img src="https://img.shields.io/badge/RAM-64GB-%230071C5.svg?&style=for-the-badge&logoColor=white" />
  <img src="https://img.shields.io/badge/nvidia-gtx%203080-%2376B900.svg?&style=for-the-badge&logo=nvidia&logoColor=white" />
  <br/><br/> 💻 PC 2<br/><br/>
  <img src="https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white" />
  <img src="https://img.shields.io/badge/intel-core%20i9%209th-%230071C5.svg?&style=for-the-badge&logo=intel&logoColor=white" />
  <img src="https://img.shields.io/badge/RAM-32GB-%230071C5.svg?&style=for-the-badge&logoColor=white" />
  <img src="https://img.shields.io/badge/nvidia-gtx%204090-%2376B900.svg?&style=for-the-badge&logo=nvidia&logoColor=white" />
</p>

<p align='center'>
  🎓 Programing Language<br/><br/>
  <img src="https://img.shields.io/badge/Python-3776AB?style=for-the-badge&logo=python&logoColor=white" />
  <img src="https://img.shields.io/badge/PyTorch-EE4C2C?style=for-the-badge&logo=PyTorch&logoColor=white" />
</p>
